#!/usr/bin/env nextflow
if (params.help) {
    log.info "----------------------------------------------------------------"
    log.info "                      USAGE                                     "
    log.info "----------------------------------------------------------------"
    log.info ""
    log.info "nextflow main.nf "
    log.info ""
    log.info "Mandatory arguments:"
    log.info "--main                   String                assembly(default) or conservation"
    log.info ""
    log.info "----------------------------------------------------------------"
    log.info "        Transcriptome assembly and lncRNA identification        "
    log.info "----------------------------------------------------------------"
    log.info "Mandatory arguments:"
    log.info "--species                String                species name, default is Human"
    log.info "Optional arguments:"
    log.info "--qctools                String                bbduk or fastp(default)"
    log.info "--TPM                    Float                 LncRNA filtering treshold (DEFAULT = 1.0)"
    log.info "--star_stranded          String                --outSAMstrandField intronMotif is default"
    log.info "--stringtie_stranded     String                --rf by default"
    log.info "----------------------------------------------------------------"
    log.info "                 Conservation analysis                          "
    log.info "----------------------------------------------------------------"
    log.info "Mandatory arguments:"
    log.info "--target_species         String                species name, sorce of lncRNAs"
    log.info "--query_species          String                species name, where to look for orthologs"
    log.info "Optional arguments:"
    log.info "--distance               String                near, medium(default) or far"
    log.info "Flags:"
    log.info "--help                                      Display this message"
    log.info ""   
    exit 1
} 

if(params.main=='assembly'){
mem_free = (Runtime.getRuntime().freeMemory())
cpu_free = Runtime.getRuntime().availableProcessors()
log.info """\
         R N A   P I P E L I N E
         =============================
         Task: ${params.main}
         species: ${params.species}
         reads : ${params.reads}
         CPU resources: ${cpu_free}
         Mem resorces: ${mem_free} bytes
         QCtool: ${params.qctools}
         Expression filtering: ${params.TPM} TPM
         =============================
         """
         .stripIndent()



Channel
    .fromFilePairs( params.reads )
    .ifEmpty { error "Cannot find any reads matching: ${params.reads}" }
    .into { read_pairs; read_pairs2 }

process ensembl_check {
    errorStrategy 'retry'
    maxRetries 10
    output:
    stdout channel_ens_name
    """
    python3 $baseDir/bin/check_ensembl.py ${params.species}
    """

}

channel_ens_name.println { "species name: $it" }

process ensembl {
    echo true
    errorStrategy 'retry'
    maxRetries 10
    output:
    file('genome.dna.toplevel.fa') into fa_pre_ch
    file("rRNAs.fasta") into rrnas_ch
    file("genes.gtf") into gtf_ch
    file("transcript_biotypes") into biotypes_ch
    file("ensembl_data.txt") into ensembl_data_ch
    """
    ensembl.py ${params.species}
    gunzip *.gz
    """
}

process ensembl_fasta_clean {
    echo true
    input:
    file('genome.fa') from fa_pre_ch
    output:
    file('genome.dna.toplevel.fa') into fa_ch
    """
    fasta_clean.py genome.fa genome.dna.toplevel.fa
    """
}

process fastqc {
    tag "$pair_id"
    publishDir  path: "$baseDir/Data/${name}/QC/", pattern: '*_logs', mode: 'copy', overwrite: true
    input:
    set pair_id, file(reads) from read_pairs
    val name from channel_ens_name
    output:
    file("fastqc_${pair_id}_logs") into qc_logs
    """
    mkdir fastqc_${pair_id}_logs
    fastqc $reads -o fastqc_${pair_id}_logs
    """
}

if(params.qctools=='bbduk'){
process bbduk {
	    tag "$pair_id"
	    publishDir path: "$baseDir/Data/${name}/QC/", pattern: '*.txt', mode: 'copy', overwrite: true
	    input:
	    set pair_id, file(reads) from read_pairs2
        val name from channel_ens_name
	    output:
	    set pair_id, file("TRIMMED/${pair_id}*") into clean_ch
            set pair_id, file("TRIMMED/${pair_id}*") into read_pairs3
	    file("${pair_id}.bbduk2_stats.txt") into trim_logs
	    script:
	    """
	    mkdir TRIMMED
	    mkdir OUT
	    mkdir OUT2
	    mkdir QA
	    bbduk.sh in=${reads[0]} in2=${reads[1]} out=OUT/${pair_id}_R1.fastq out2=OUT/${pair_id}_R2.fastq ref=/opt/conda/envs/nf-core-rnaseq/opt/bbmap-38.73-0/resources/adapters.fa ktrim=r k=23 mink=11 hdist=1 tpe tbo minlen=50 stats=${pair_id}.bbduk2_stats.txt
	    bbduk.sh in=OUT/${pair_id}_R1.fastq in2=OUT/${pair_id}_R2.fastq out=OUT2/${pair_id}_R1.fastq out2=OUT2/${pair_id}_R2.fastq qtrim=r trimq=10 minlen=50
	    bbduk.sh in=OUT2/${pair_id}_R1.fastq in2=OUT2/${pair_id}_R2.fastq out=TRIMMED/${pair_id}_R1.fastq out2=TRIMMED/${pair_id}_R2.fastq maq=10
	    cp TRIMMED/${pair_id}_R1.fastq QA/${pair_id}_R1_trimmed.fastq
	    cp TRIMMED/${pair_id}_R2.fastq QA/${pair_id}_R2_trimmed.fastq
	    """
	}

}else if(params.qctools =='fastp'){

process Run_fastp {

            tag "$pair_id"
            publishDir "$baseDir/Data/${name}/QC/", pattern: '*_fastp.html', mode: 'copy', overwrite: true

            input:
            set pair_id, file(reads) from read_pairs2
            val name from channel_ens_name
            output:
            file "*.json" into trim_logs
            set pair_id, file("TRIMMED/${pair_id}*") into clean_ch
            set pair_id, file("TRIMMED/${pair_id}*") into read_pairs3
            file "*"
            script:
            """
            mkdir TRIMMED
            fastp -i ${reads[0]}  -I ${reads[1]} -o TRIMMED/${pair_id}_R1_trimmed.fastq -O TRIMMED/${pair_id}_R2_trimmed.fastq -h ${pair_id}_fastp.html -j ${pair_id}_fastp.json
            """
            }

}


process bowtie_rRNA_index {
    tag "Bowtie rRNA index"

    input:
    file(rRNAs) from rrnas_ch

    output:
    file("index/*.*") into bowtie_index_ch
    """
    mkdir index
    bowtie2-build ${rRNAs} index/rRNAs
    """
}

process bowtie_rRNA {
    tag "Bowtie rRNA $pair_id"
    cpus = (Runtime.getRuntime().availableProcessors() / 4)
    input:
    file(index) from bowtie_index_ch
    set pair_id, file(trimed) from clean_ch

    output:
    set pair_id, file("unmapped/${pair_id}*") into bowtie_umapped_ch

    """
    mkdir unmapped
    bowtie2 -t -p ${task.cpus} -X 1000 -1 ${trimed[0]} -2 ${trimed[1]} -x rRNAs --fast --un-conc unmapped/${pair_id}.fastq > /dev/null
    mv unmapped/${pair_id}.1.fastq unmapped/${pair_id}_clean_R1.fastq
    mv unmapped/${pair_id}.2.fastq unmapped/${pair_id}_clean_R2.fastq
    """
}

process star_index {
    tag "Star index"
    cpus = (Runtime.getRuntime().availableProcessors())
    input:
    file(genes) from gtf_ch
    file(genome) from fa_ch

    output:
    file "genome_index" into star_index
    """
    mkdir genome_index
    STAR --runThreadN ${task.cpus} --genomeDir genome_index --runMode genomeGenerate --genomeFastaFiles ${genome} --sjdbGTFfile ${genes} --sjdbOverhang 100
    """
}


process star_align {
    tag "Star $pair_id"
    cpus = (Runtime.getRuntime().availableProcessors())
    input:
    set pair_id, file(reads) from bowtie_umapped_ch
    file index from star_index.collect()

    output:
    set pair_id, file('star') into star_mapped
    file("star/${pair_id}Log.final.out") into star_log_ch

    """
    mkdir star
    STAR --runThreadN ${task.cpus} --limitBAMsortRAM 19261643698 --genomeDir ${index} --readFilesIn ${reads[0]} ${reads[1]} --outFileNamePrefix star/${pair_id} --outSAMattributes All --outSAMattrIHstart 0 --outSAMtype BAM SortedByCoordinate --outSAMunmapped Within ${params.star_stranded} --outFilterIntronMotifs RemoveNoncanonical --outFilterType BySJout --outFilterMultimapNmax 20 --alignSJoverhangMin 8 --alignSJDBoverhangMin 1 --outFilterMismatchNmax 999 --outFilterMismatchNoverLmax 0.04 --alignIntronMin 20 --alignIntronMax 1000000 --alignMatesGapMax 1000000 --twopassMode Basic --chimSegmentMin 12 --chimJunctionOverhangMin 12 --chimSegmentReadGapMax 3 > ${pair_id}_log_STAR.txt
    """
}


process stringtie {
    tag "Stringtie $pair_id"
    cpus = (Runtime.getRuntime().availableProcessors())
    input:
    set pair_id, file(star) from star_mapped
    file(genes) from gtf_ch
    file "*"
    output:
    file("GTF/${pair_id}.gtf") into stringtie_ch
    file('*_log_stringtie')  into stringtie_log_ch
    """
    mkdir GTF
    stringtie star/${pair_id}Aligned.sortedByCoord.out.bam -o GTF/${pair_id}.gtf -v -p ${task.cpus} ${params.stringtie_stranded} -G ${genes} -A ${pair_id}_abundance.txt > ${pair_id}_log_stringtie
    """
}

stringtie_ch.into {
  stringtie_ch2
  listGTF
}

process createList {
        tag "${listGTF}"

        input:
        file listGTF from listGTF.collect()

        output:
        file "mergeList.txt" into mergeList

        script:
        """
        touch mergeList.txt
        ls *.gtf > mergeList.txt
        """
}



process stringtie_merged {
    tag "Stringtie merging"
    cpus = (Runtime.getRuntime().availableProcessors())

    input:
    file "mergeList.txt" from mergeList
    file(GTF) from stringtie_ch2.collect()

    output:
    file('merged_transcriptomes.gtf') into stringtie_merged_ch
    """
    stringtie --merge -p ${task.cpus} -T 0 -F 0 -v -o merged_transcriptomes.gtf mergeList.txt
    """
}


process filter_strand {
   tag "Filtering by strand"

   input:
   file(transcriptome) from stringtie_merged_ch

   output:
   file('transcriptome_pre.gtf')  into strand_filtered_ch

   """
   filter_by_strand.py ${transcriptome}
   """
}

process cuffcompare {
   tag "cuffcompare"

   input:
   file(transcriptome) from strand_filtered_ch
   file(genes) from gtf_ch

   output:
   file('all.combined.gtf')  into cuffcompare_ch

   """
   cuffcompare -r ${genes} -R -o all -G ${transcriptome}
   """
}

process filter_classcode {
   tag "Filtering by classcode"
   publishDir "$baseDir/Data/${name}/", pattern: '*_final.gtf', mode: 'copy', overwrite: true

   input:
   file(transcriptome_combined) from cuffcompare_ch
   file(transcriptome_pre) from strand_filtered_ch
   val name from channel_ens_name
   output:
   file('*_final.gtf')  into assembled_gtf_ch

   """
   filter_class_codes.py ${transcriptome_combined} ${transcriptome_pre} ${name}_final.gtf
   """
}


process make_a_fasta {
   tag "Making a fasta"
   publishDir "$baseDir/Data/${name}/", pattern: '*_final.fasta', mode: 'copy', overwrite: true

   input:
   file(transcriptome) from assembled_gtf_ch
   file(genome) from fa_ch
   val name from channel_ens_name
   output:
   file('*_final.fasta')  into assembled_fa_ch

   """
   gffread -g ${genome} -w ${name}_final.fasta ${transcriptome}
   """
}

process expression_index {
   tag "expression_index"
   input:
   val name from channel_ens_name
   file(transcriptome) from assembled_fa_ch
   output:
   file("${name}_index") into salmon_index_ch
   """
   salmon index -t ${transcriptome} -i ${name}_index
   """
}



process expression_estimation {
   tag "expression_estimation ${pair_id}"
   cpus = (Runtime.getRuntime().availableProcessors() / 2)
   input:
   val name from channel_ens_name
   file(transcriptome)  from salmon_index_ch
   set pair_id, file(reads) from read_pairs3
   output:
   file("quants/${pair_id}") into quants_ch
   file("quants/${pair_id}") into quants_ch_qc
   """
   mkdir quants
   salmon quant -i ${transcriptome} -l A -1 ${reads[0]} -2 ${reads[1]} -p ${task.cpus} --validateMappings -o quants/${pair_id}
   """
}

process expression_processing {
   tag "expression_processing"
   publishDir path: "$baseDir/Data/${name}/expression/", pattern: "expression.tsv", mode: 'copy', overwrite: true
   input:
   file quants from quants_ch.collect()
   output:
   file('expression.tsv') into expression_final_ch
   script:
   """
   expression_salmon.py
   """
}

process make_a_fasta_identification {
   tag "Making new fasta"

   input:
   file(assembled_gtf) from assembled_gtf_ch
   file(genome) from fa_ch
   val name from channel_ens_name
   output:
   file('*_new.fasta') into processed_fasta_ch
   file('gene2transcript/') into gene2transcript_ch

   """
   mkdir gene2transcript
   gtf2seq.py ${genome} ${assembled_gtf} ${name}_new.fasta gene2transcript/${name}_gene2transcript
   """
}

process cuffcompare_identification {
   tag "Identification"

   input:
   file(assembled_gtf) from assembled_gtf_ch
   file(genes) from gtf_ch
   val name from channel_ens_name
   output:
   file('Cuffcompare/*.combined.gtf') into cuffcompare_ident_ch
   """
   mkdir Cuffcompare
   cuffcompare -r ${genes} -R -o Cuffcompare/${name} -C -G  ${assembled_gtf}
   """
}

process filtering_stage {
   tag "lncRNA filtering"

   input:
   file(assembled_gtf) from assembled_gtf_ch.collect()
   file(cuffcompare_gtf) from cuffcompare_ident_ch
   file(fasta) from processed_fasta_ch
   file(biotypes) from biotypes_ch
   val name from channel_ens_name
   output:
   file('ADDITIONAL/') into rna_filtered_ch
   file('FASTA/') into filtered_fasta_ch
   file('FASTA/*_pre-filtered.fasta') into fasta_candidates_ch
   """
   mkdir ADDITIONAL
   mkdir FASTA
   filter.py ${name} ${assembled_gtf} ${fasta} ${cuffcompare_gtf} ${biotypes}
   """
}

process transdecoder_stage {


   input:
   file('FASTA') from filtered_fasta_ch
   val name from channel_ens_name
   output:
   file('*_pre-filtered_2.fasta') into transdecoder_ch
   """
   mkdir TRANSDECODER
   TransDecoder.LongOrfs -t ./FASTA/${name}_pre-filtered.fasta -m 100 -S
   mv ${name}_pre-filtered.fasta.transdecoder_dir TRANSDECODER/
   transdecoder_filter.py ${name}
   """
}

process CPC {
   tag "CPC - using second enviroment"
   echo true
   input:
   file(prefiltered) from transdecoder_ch
   val name from channel_ens_name
   output:
   file('result_*') into cpc_ch
   """
   mkdir HELPER
   mkdir HELPER/${name}
   source /opt/conda/bin/activate /opt/conda/envs/cpc2/ && python /opt/conda/envs/cpc2/bin/CPC2.py -i ${prefiltered}  -o result_${name}
   echo "Done"
   """

}


process candidates {
   tag "difining lncRNA candidates"
   echo true
   publishDir "$baseDir/Data/${name}/", pattern: 'PREDICTIONS', mode: 'copy', overwrite: true

   input:
   file(ensembl) from ensembl_data_ch
   file(fasta) from fasta_candidates_ch
   file(prefiltered) from transdecoder_ch
   file(cpc) from cpc_ch
   file(assembled_gtf) from assembled_gtf_ch
   file(combined) from cuffcompare_ident_ch
   file(expression) from expression_final_ch
   val name from channel_ens_name
   output:
   file('PREDICTIONS/') into lncRNAs_ch
   file("PREDICTIONS/${name}_lncrnas_data.txt") into lncRNAs_report
   """
   mkdir PREDICTIONS
   candidates.py ${name} ${prefiltered} ${fasta} ${cpc} ${assembled_gtf} ${combined} ${ensembl} ${expression} ${params.TPM}
   """

}

params.multiqc_config = "$baseDir/multiqc_config.yaml"
Channel.fromPath(params.multiqc_config, checkIfExists: true).set { ch_config_for_multiqc }

process multiqc {
    publishDir path: "$baseDir/Data/${name}/QC/", pattern: 'multiqc_report.*', mode: 'copy', overwrite: true
    input:
    file multiqc_config from ch_config_for_multiqc
    file ('salmon/*') from quants_ch_qc.collect().ifEmpty([])
    file ('fastqc/*') from qc_logs.collect().ifEmpty([])
    file ('stringtie/*') from stringtie_log_ch.collect().ifEmpty([])
    file ('star/*') from star_log_ch.collect().ifEmpty([])
    file ('qc/*') from trim_logs.collect().ifEmpty([])
    val name from channel_ens_name
    output:
    file "multiqc_report.html" into multiqc_report
    file "multiqc_data"

    script:
    """
    multiqc .
    """
}

Channel
    .fromPath("$baseDir/bin/sweetviz_patched", type: 'dir')
    .set { sweetviz_patched_ch }

process make_assembly_report {
        echo true
        publishDir "$baseDir/Data/${name}/", pattern: "${name}_lncRNAS.html", mode: 'copy', overwrite: true
        input:
        file("${name}_lncrnas_data.txt") from lncRNAs_report
        file('/sweetviz_patched') from sweetviz_patched_ch
        val name from channel_ens_name
        output:
        file("${name}_lncRNAS.html")        
        script:
        """
        source /opt/conda/bin/activate /opt/conda/envs/sweetviz/ && cd ./sweetviz_patched && pip install . && cd ..
        sweetviz_run.py report assembly ${name}
        """
}

}

if(params.main =='conservation'){
  mem_free = (Runtime.getRuntime().freeMemory())
  cpu_free = Runtime.getRuntime().availableProcessors()


query = "${params.query_species}"
target = "${params.target_species}"
log.info """\
         R N A   P I P E L I N E
         =============================
         Task: ${params.main}
         query: ${params.species}
         target: ${params.target_species}
         CPU resources: ${cpu_free}
         Mem resorces: ${mem_free} bytes
         distance: ${params.distance}
         =============================
         """
         .stripIndent()


query_lncRNA_gtf = Channel.fromPath( "${baseDir}/Data/${query}/PREDICTIONS/${query}_lncrnas.gtf" )
query_lncRNA_data = Channel.fromPath( "${baseDir}/Data/${query}/PREDICTIONS/${query}_lncrnas_data.txt" )


process ensembl_query {
    echo true
    errorStrategy 'retry'
    maxRetries 10

    output:
    file('query.fa') into query_fa_ch
    file("genes.gtf") into query_gtf_ch
    """
    orthology_ensembl.py ${query}
    gunzip *.gz
    fasta_clean.py genome.dna.toplevel.fa genome.fa
    orthology_chromosome_names.py genome.fa query.fa
    rm genome.fa genome.dna.toplevel.fa
    """
}

process ensembl_target {
    echo true
    errorStrategy 'retry'
    maxRetries 10
    input:

    output:
    file('target.fa') into target_fa_ch
    file("genes.gtf") into target_gtf_ch
    """
    orthology_ensembl.py ${target}
    gunzip *.gz
    fasta_clean.py genome.dna.toplevel.fa genome.fa
    orthology_chromosome_names.py genome.fa target.fa
    rm genome.fa genome.dna.toplevel.fa
    """
}

process query_gtf_processing {

    input:
    file(GTF) from query_lncRNA_gtf
    file(DATA) from query_lncRNA_data
    output:
    file("${query}_formatted.gtf") into query_gtf_formatted_ch
    """
    orthology_gtf.py ${GTF} ${DATA} ${query}
    """
}
process query_gtf_merged {
    input:
    file(gtf_formatted) from query_gtf_formatted_ch
    file(gtf_file) from query_gtf_ch
    output:
    file("${query}.gtf") into query_gtf_merged_ch
    """
    cat ./${gtf_file} ./${gtf_formatted} > ${query}.gtf
    """
}

process query_gtf_processing_2 {
    input:
    file(gtf_formatted) from query_gtf_merged_ch
    file(fasta) from query_fa_ch
    output:
    file("${query}.fa") into query_fa_processed_ch
    file('processed.gtf') into query_gtf_processed_ch
    """
    orthology_convert_ensembl.py ${gtf_formatted} ${fasta}
    mv processed.fa ${query}.fa
    """
}
process query_testing {
    input:
    file('processed.gtf') from query_gtf_processed_ch
    output:
    file("${query}.gtf") into query_gtf_final
    file('log.txt') into query_gtf_log
    file("SLNCKY/annotations/${query}*") into query_f_ch
    """
    mkdir ./SLNCKY/
    mkdir ./SLNCKY/annotations/
    orthology_annotations.py ${query}
    head ./SLNCKY/annotations/*.* > log.txt
    cp ./processed.gtf ${query}.gtf
    """
}




cross_f = file( "$baseDir/Data/$query/${target}.${query}.over.chain.gz" )
if(cross_f.exists() == true){
    println "File exists, no need to make a new one"
    Channel
        .fromPath( "$baseDir/Data/$query/${target}.${query}.over.chain.gz" )
        .set { chain_ch }
}
else{

println "Making new cross-species alignments"


process fa_2bit {
    input:
    file('query.fa') from query_fa_ch
    file('target.fa') from target_fa_ch
    output:
    file("${query}.2bit") into query_2bit_ch
    file("${target}.2bit") into target_2bit_ch
    """
    faToTwoBit ./query.fa ./${query}.2bit
    faToTwoBit ./target.fa ./${target}.2bit
    """
}


query_2bit_ch.into { query_2bit_ch_1; query_2bit_ch_2 }
target_2bit_ch.into { target_2bit_ch_1; target_2bit_ch_2 }



distance_par = [ "near":"NEAR", "medium":"MAM4", "far":"MAM4", "medium_fast":"MAM4", "far_fast":"MAM4" ]
distance_par2 = [ "near":"-minScore=5000 -linearGap=medium", "medium":"-minScore=3000 -linearGap=medium", "far":"-minScore=5000 -linearGap=loose", "medium_fast":"-minScore=3000 -linearGap=medium", "far_fast":"-minScore=5000 -linearGap=loose" ]
distance_par3 = [ "near":"-m10", "medium":"-m100", "far":"-m100",  "medium_fast":"-m10", "far_fast":"-m10"  ]
distance_par4 = [ "near":"-W99", "medium":"", "far":"",  "medium_fast":"-W99", "far_fast":"-W99" ]
distance_seed = distance_par["${params.distance}"]
distance_axt = distance_par2["${params.distance}"]
distance_sense= distance_par3["${params.distance}"]
distance_fast= distance_par4["${params.distance}"]

process chain_preparation_dividing_fasta {
    input:
    file("${target}.fa") from target_fa_ch
   
    output:
    file("chr*.fa") into splitted_fasta
    """
    pip install pyfaidx
    faidx -x ${target}.fa
    ls -l
    """
}


process chain_preparation_index {
    input:
    file("*") from splitted_fasta.collect()
   
    output:
    file("${target}-${distance_seed}*.*") into last_index
    """
    lastdb -P0 ${distance_fast} -u${distance_seed} -R01 ${target}-${distance_seed}  *.fa
    """
}

process chain_preparation_training {
    input:
    file("${query}.fa") from query_fa_ch
    file('*') from last_index   
    output:
    file("${target}-${query}.mat") into last_matrix
    """
    last-train -P0 --revsym --matsym --gapsym -E0.05 -C2 ${target}-${distance_seed} ${query}.fa > ${target}-${query}.mat
    """
}

process chain_preparation_aligning {
    memory '120 GB'
    input:
    file("${query}.fa") from query_fa_ch
    file("*") from last_index
    file("${target}-${query}.mat") from last_matrix   
    output:
    file("${target}-${query}-1.maf") into last_maf
    """
    lastal -P2 -i3G ${distance_sense} -E0.05 -C2 -p ${target}-${query}.mat ${target}-${distance_seed} ${query}.fa | last-split -m1 > ${target}-${query}-1.maf
    """
}


process chain_preparation_psl {
    input:
    file("input.maf") from last_maf
    output:
    file("alignments.psl") into last_psl
    """
    ls -l
    maf-convert psl input.maf > alignments.psl 
    """
}

process chain_preparation_chain {
    input:
    file("alignments.psl") from last_psl
    file("${target}-${query}.mat") from last_matrix
    file("${query}.2bit") from query_2bit_ch_1
    file("${target}.2bit") from target_2bit_ch_2 
    output:
    file("${target}.${query}.all.chain") into chain_all_ch
    """
    axtChain -psl ${distance_axt} -scoreScheme=${target}-${query}.mat alignments.psl ${target}.2bit ${query}.2bit ${target}.${query}.all.chain
    """
}



process chain_netting {
    input:
    publishDir path: "$baseDir/Data/${query}/", pattern: "${target}.${query}.over.chain.gz", mode: 'copy', overwrite: true
    file("${query}.2bit") from query_2bit_ch_2
    file("${target}.2bit") from target_2bit_ch_2
    file("${target}.${query}.all.chain") from chain_all_ch
    output:
    file("${target}.${query}.over.chain.gz") into chain_ch
    """
    twoBitInfo ${query}.2bit ${query}.chromInfo
    twoBitInfo ${target}.2bit ${target}.chromInfo
    mkdir net
    chainNet ${target}.${query}.all.chain ${target}.chromInfo ${query}.chromInfo net/all.net /dev/null
    netChainSubset net/all.net ${target}.${query}.all.chain ${target}.${query}.over.chain
    gzip -k ./${target}.${query}.over.chain
    """
}


}
target_lncRNA_gtf = Channel.fromPath( "${baseDir}/Data/${target}/PREDICTIONS/${target}_lncrnas.gtf" )
target_lncRNA_data = Channel.fromPath( "${baseDir}/Data/${target}/PREDICTIONS/${target}_lncrnas_data.txt" )

process target_gtf_processing {

    input:
    file(GTF) from target_lncRNA_gtf
    file(DATA) from target_lncRNA_data
    output:
    file("${target}_formatted.gtf") into target_gtf_formatted_ch
    """
    orthology_gtf.py ${GTF} ${DATA} ${target}
    """
}

target_gtf_formatted_ch.into { target_gtf_formatted_ch_1; target_gtf_formatted_ch_2 }

process target_gtf_merged {
    input:
    file(gtf_formatted) from target_gtf_formatted_ch_1
    file(gtf_file) from target_gtf_ch
    output:
    file("${target}.gtf") into target_gtf_merged_ch
    """
    cat ./${gtf_file} ./${gtf_formatted} > ${target}.gtf
    """
}

process target_gtf_processing_2 {
    input:
    file(gtf_formatted) from target_gtf_merged_ch
    file(fasta) from target_fa_ch
    output:
    file('processed.gtf') into target_gtf_processed_ch
    file("${target}.fa") into target_fa_processed_ch
    """
    orthology_convert_ensembl.py ${gtf_formatted} ${fasta}
    mv processed.fa ${target}.fa
    """
}
process target_testing {
    input:
    file('processed.gtf') from target_gtf_processed_ch
    output:
    file("${target}.gtf") into target_gtf_final
    file("SLNCKY/annotations/${target}*") into target_f_ch
    file('log.txt') into target_gtf_log
    """
    mkdir ./SLNCKY/
    mkdir ./SLNCKY/annotations/
    orthology_annotations.py ${target}
    head ./SLNCKY/annotations/*.* > log.txt
    cp ./processed.gtf ${target}.gtf
    """
}




target_fa_processed_ch.into { target_fa_processed_ch_1; target_fa_processed_ch_2 }

Channel
    .fromPath("$baseDir/bin/SLNCKY/slncky.v1.0", type: 'file')
    .collect()
    .set { slncky_ch }
Channel
    .fromPath("$baseDir/bin/SLNCKY/alignTranscripts1.0", type: 'file')
    .collect()
    .set { slncky_ch2 }





process slncky_config {
    output:
    file('annotations.config') into config_final_ch
    """
    touch annotations.config
    echo ">${target}" >> annotations.config
    echo "CODING=./${target}_coding.bed" >> annotations.config
    echo "ORTHOLOG=${query}" >> annotations.config
    echo "LIFTOVER=./${target}.${query}.over.chain.gz" >> annotations.config
    echo "GENOME_FA=./${target}.fa" >> annotations.config
    echo "NONCODING=./${target}_lncRNAs.bed" >> annotations.config
    echo "GENESYMBOL=./${target}_gene_names.txt" >> annotations.config
    echo "MIRNA=./${target}_miRNAs.bed" >> annotations.config
    echo "SNORNA=./${target}_snoRNAs.bed \n" >> annotations.config
    echo ">${query}" >> annotations.config
    echo "CODING=./${query}_coding.bed" >> annotations.config
    echo "ORTHOLOG=${target}" >> annotations.config
    echo "GENOME_FA=./${query}.fa" >> annotations.config
    echo "NONCODING=./${query}_lncRNAs.bed" >> annotations.config
    echo "GENESYMBOL=./${query}_gene_names.txt" >> annotations.config
    echo "MIRNA=./${query}_miRNAs.bed" >> annotations.config
    echo "SNORNA=./${query}_snoRNAs.bed \n" >> annotations.config
    """
}


slncky_ch
    .merge( slncky_ch2, target_f_ch, query_f_ch, config_final_ch )
    .collect()
    .set { slncky_ch_f }



process bed_preparation {
    input:
    file("${target}.gtf") from target_gtf_formatted_ch_2
    file("${target}.fa") from target_fa_processed_ch_1
    output:
    file('lnc_pool/*') into newbeds_ch
    """
    mkdir lnc_pool
    orthology_GTF2BED.py ${target}.gtf lnc_pool/transcripts.bed ${target}.fa
    cd ./lnc_pool
    bedtools sort -chrThenSizeA -i transcripts.bed > transcripts_sorted.bed 
    split -l 2500 transcripts_sorted.bed segment_
    rm -f  transcripts_sorted.bed && rm -f  transcripts.bed
    """
}


newbeds_ch
    .flatten() 
    .view()
    .into { newbeds_ch2; newbeds_ch3 }

newbeds_ch3
    .map { file -> file.baseName }
    .set { newbeds_ch_ids }

slncky_ch_f
    .merge( chain_ch, query_fa_processed_ch, target_fa_processed_ch_2 )
    .collect()
    .set { slncky_ch_f2 }

newbeds_ch2
    .combine( slncky_ch_f2 )
    .set { slncky_ch_final }


process slncky_run {
    cpus = Runtime.getRuntime().availableProcessors()
    maxForks = 1
    input:
    val(bedId) from newbeds_ch_ids
    file("*") from slncky_ch_final
    output:
    file("${bedId}.${query}.orthologs.txt") into orthologs_ch
    file("${bedId}.${query}.orthologs.top.txt") into orthologs_ch_top
    """
    ls -l
    mv segment_* transcripts.bed
    samtools faidx ${query}.fa
    samtools faidx ${target}.fa
    chmod +x ./slncky.v1.0 && chmod +x ./alignTranscripts1.0
    ./slncky.v1.0 --no_filter --minMatch=0.01 --no_orf --pad=100000 --config ./annotations.config --threads=${task.cpus} ./transcripts.bed ${target} ${query}
     mv ${query}.orthologs.top.txt ${bedId}.${query}.orthologs.top.txt
     mv ${query}.orthologs.txt ${bedId}.${query}.orthologs.txt
    """
}

process top_result {
        echo true
        publishDir "$baseDir/Data/${query}/", pattern: "${target}.${query}.orthologs.top.final.txt", mode: 'copy', overwrite: true
        input:
        file toporthologs from orthologs_ch_top.collect()
        output:
        file("${target}.${query}.orthologs.top.final.txt")
        file("${target}.${query}.orthologs.top.final.txt") into cons_report_ch
        script:
        """
        cat *.txt > ${target}.${query}.orthologs.top.final.txt
        """
}

process all_result {
        echo true
        publishDir "$baseDir/Data/${query}/", pattern: "${target}.${query}.orthologs.final.txt", mode: 'copy', overwrite: true
        input:
        file toporthologs from orthologs_ch.collect()
        output:
        file("${target}.${query}.orthologs.final.txt")
        script:
        """
        cat *.txt > ${target}.${query}.orthologs.final.txt
        """
}

Channel
    .fromPath("$baseDir/bin/sweetviz_patched", type: 'dir')
    .set { sweetviz_patched_ch }


process make_report_conservation {
        echo true
        publishDir "$baseDir/Data/${query}/", pattern: "${target}-${query}_conservation.html", mode: 'copy', overwrite: true
        input:
        file("${target}.${query}.orthologs.top.final.txt") from cons_report_ch
        file('/sweetviz_patched') from sweetviz_patched_ch
        output:
        file("${target}-${query}_conservation.html")
        
        script:
        """
        source /opt/conda/bin/activate /opt/conda/envs/sweetviz/ && cd ./sweetviz_patched && pip install . && cd ..
        sweetviz_run.py report conservation ${target}-${query} 
        """
}

}
if(params.main =='comparison'){

if(params.data_type=="assembly"){

Channel
    .fromPath("$baseDir/bin/sweetviz_patched", type: 'dir')
    .set { sweetviz_patched_ch }

Channel
    .fromPath("$baseDir/Data/${params.species1}/PREDICTIONS/${params.species1}_lncrnas_data.txt", type: 'file')
    .collect()
    .set { lnc_set_ch_1 }
Channel
    .fromPath("$baseDir/Data/${params.species2}/PREDICTIONS/${params.species2}_lncrnas_data.txt", type: 'file')
    .collect()
    .set { lnc_set_ch_2 }

process make_comparison_assembly {
        echo true
        publishDir "$baseDir/Data/", pattern: "${params.species1}_${params.species2}_lncRNAS.html", mode: 'copy', overwrite: true
        input:
        file("${params.species1}_lncrnas_data.txt") from lnc_set_ch_1
	file("${params.species2}_lncrnas_data.txt") from lnc_set_ch_2
        file('/sweetviz_patched') from sweetviz_patched_ch
        output:
        file("${params.species1}_${params.species2}_lncRNAS.html")
        script:
        """
        source /opt/conda/bin/activate /opt/conda/envs/sweetviz/ && cd ./sweetviz_patched && pip install . && cd ..
        sweetviz_run.py compare assembly ${params.species1} ${params.species2}
        """
}
}
if(params.data_type=="conservation"){

Channel
    .fromPath("$baseDir/bin/sweetviz_patched", type: 'dir')
    .set { sweetviz_patched_ch }

Channel
    .fromPath("$baseDir/Data/${params.query1}/${params.target1}.${params.query1}.orthologs.top.final.txt", type: 'file')
    .collect()
    .set { lnc_set_ch_1 }
Channel
    .fromPath("$baseDir/Data/${params.query2}/${params.target2}.${params.query2}.orthologs.top.final.txt", type: 'file')
    .collect()
    .set { lnc_set_ch_2 }

process make_comparison_conservation {
        echo true
        publishDir "$baseDir/Data/", pattern: "${params.target1}-${params.query1}_${params.target2}-${params.query2}_conservation.html", mode: 'copy', overwrite: true
        input:
        file("${params.target1}.${params.query1}.orthologs.top.final.txt") from lnc_set_ch_1
        file("${params.target2}.${params.query2}.orthologs.top.final.txt") from lnc_set_ch_2
        file('/sweetviz_patched') from sweetviz_patched_ch
        output:
        file("${params.target1}-${params.query1}_${params.target2}-${params.query2}_conservation.html")        
        script:
        """
        source /opt/conda/bin/activate /opt/conda/envs/sweetviz/ && cd ./sweetviz_patched && pip install . && cd ..
        sweetviz_run.py compare conservation ${params.target1}-${params.query1} ${params.target2}-${params.query2}
        """
}
}
}
